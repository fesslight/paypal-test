<?php

namespace frontend\models;

//use common\components\VarDumper;
use Yii;
use yii\base\Model;
//use common\components\Helpers;
//use common\models\ManPayments;
//use yii\helpers\ArrayHelper;

/**
 * PayForm is the model behind the contact form.
 */
class PayForm extends Model
{
    const DEBUG = false;
    const COUNT_SUM_PAID = 10;

    const NOTIFY = '/payment/ipn';
    const SUCCESS = '/payment/success';
    const CANCEL = '/payment/cancel';

    const DEFAULT_USE_SANDBOX = true;
    const DEFAULT_CURRENCY_CODE = 'USD';
    const DEFAULT_MERCHANT_ID = 'G6CGX4JVXW3YW';
    const DEFAULT_ACTION_SANDBOX = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
    const DEFAULT_ACTION_LIVE = 'https://www.paypal.com/cgi-bin/webscr';

    public $action;
    public $params;
    public $settings;

    public $ipn_request;
    public $ipn_data_custom;

    public $cmd = '_xclick';
    public $notify_url;
    public $business;
    public $lc = 'US';
    public $item_name;
    public $item_number;
    public $amount;
    public $invoice;
    public $custom;
    public $currency_code;
    public $button_subtype = 'services';
    public $no_note = 1;
    public $no_shipping = 1;
    public $rm = 2;
    public $return;
    public $cancel_return;
    public $undefined_quantity = 0;
    public $bn = 'PP-BuyNowBF:btn_buynow_LG.gif:NonHosted';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cmd', 'business', 'lc', 'item_name', 'item_number', 'amount', 'currency_code', 'button_subtype', 'no_note', 'no_shipping', 'undefined_quantity', 'bn'], 'safe'],
        ];
    }

    public function setParams($user_id = null)
    {
        if(!isset($user_id))
            $user_id = Yii::$app->user->id;

        $this->action = self::DEFAULT_USE_SANDBOX ? self::DEFAULT_ACTION_SANDBOX : self::DEFAULT_ACTION_LIVE;
        $this->business = self::DEFAULT_USE_SANDBOX ? self::DEFAULT_MERCHANT_ID : self::DEFAULT_MERCHANT_ID;
        $this->currency_code = self::DEFAULT_CURRENCY_CODE;

        $this->notify_url = Yii::$app->urlManager->createAbsoluteUrl(self::NOTIFY);
        $this->return = Yii::$app->urlManager->createAbsoluteUrl(self::SUCCESS);
        $this->cancel_return = Yii::$app->urlManager->createAbsoluteUrl(self::CANCEL);
        $this->custom = json_encode(['site_user_id' => $user_id]);

        $this->item_name = "PAY";
        $this->settings = "Setting";
    }

    public function getPostIPN()
    {
        // Read POST data
        $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $key_val) {
            $key_val = explode('=', $key_val);
            if (count($key_val) == 2)
                $myPost[$key_val[0]] = urldecode($key_val[1]);
        }

        $this->ipn_data_custom = json_decode($myPost['custom'], true);

        // read the post from PayPal system and add 'cmd'
        $this->ipn_request = 'cmd=_notify-validate';
        if (function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        } else {
            $get_magic_quotes_exists = false;
        }

        foreach ($myPost as $key => $value) {
            if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }
            $this->ipn_request .= "&$key=$value";
        }

        return $this->ipn_request;
    }

    public function sendRequest()
    {
        $ch = curl_init($this->action);
        if ($ch == FALSE) {
            return FALSE;
        }

        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getPostIPN());
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        if (self::DEBUG == true) {
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
        }

        // Set TCP timeout to 30 seconds
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

        $res = curl_exec($ch);
        curl_close($ch);

        return $res;
    }
}
