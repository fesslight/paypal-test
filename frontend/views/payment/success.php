<?php
use yii\helpers\Html;
?>


<?php if($model->status == \frontend\models\PayForm::SUCCESS):?>
    <h4>You are success paid</h4>
    <?= Html::a('Download', 'download-zip', ['class' => 'btn btn-lg btn-success']) ?>
<?php else:?>
    <h4>You paid status in process (please wait)</h4>
<?php endif;?>