<?php
/* @var $this yii\web\View */
/* @var $model \frontend\models\PayForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<h1>Please make a payment</h1>

<?php $form = ActiveForm::begin([
    'action' => $model->action,
]); ?>

    <?= $form->field($model, 'cmd')->hiddenInput(['name' => 'cmd', 'readonly' => true])->label(false) ?>
    <?= $form->field($model, 'notify_url')->hiddenInput(['name' => 'notify_url', 'readonly' => true])->label(false) ?>
    <?= $form->field($model, 'business')->hiddenInput(['name' => 'business', 'readonly' => true])->label(false) ?>
    <?= $form->field($model, 'lc')->hiddenInput(['name' => 'lc', 'readonly' => true])->label(false) ?>
    <?= $form->field($model, 'item_name')->hiddenInput(['name' => 'item_name', 'readonly' => true])->label(false) ?>
    <?= $form->field($model, 'amount')->hiddenInput(['name' => 'amount', 'readonly' => true])->label(false) ?>
    <?= $form->field($model, 'custom')->hiddenInput(['name' => 'custom', 'readonly' => true])->label(false) ?>
    <?= $form->field($model, 'currency_code')->hiddenInput(['name' => 'currency_code', 'readonly' => true])->label(false) ?>
    <?= $form->field($model, 'button_subtype')->hiddenInput(['name' => 'button_subtype', 'readonly' => true])->label(false) ?>
    <?= $form->field($model, 'no_note')->hiddenInput(['name' => 'no_note', 'readonly' => true])->label(false) ?>
    <?= $form->field($model, 'no_shipping')->hiddenInput(['name' => 'no_shipping', 'readonly' => true])->label(false) ?>
    <?= $form->field($model, 'undefined_quantity')->hiddenInput(['name' => 'undefined_quantity', 'readonly' => true])->label(false) ?>
    <?= $form->field($model, 'rm')->hiddenInput(['name' => 'rm', 'readonly' => true])->label(false) ?>
    <?= $form->field($model, 'return')->hiddenInput(['name' => 'return', 'readonly' => true])->label(false) ?>
    <?= $form->field($model, 'cancel_return')->hiddenInput(['name' => 'cancel_return', 'readonly' => true])->label(false) ?>
    <?= $form->field($model, 'bn')->hiddenInput(['name' => 'bn', 'readonly' => true])->label(false) ?>

    <?= Html::submitButton('Pay', ['class' => 'btn btn-success']) ?>

<?php ActiveForm::end(); ?>
