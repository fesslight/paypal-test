<?php

namespace frontend\controllers;

use Yii;
use common\models\User;
use frontend\models\PayForm;
use common\models\Payments;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class PaymentController extends Controller
{
    public function beforeAction($action)
    {
        $actions = ['success', 'cancel', 'ipn'];

        // Disable CSRF validation for PayPal
        if(ArrayHelper::isIn(Yii::$app->controller->action->id ,$actions)){
            Yii::$app->controller->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actionIpn()
    {
        if(Yii::$app->request->isPost){
            $post = Yii::$app->request->post();

            $custom = json_decode($post['custom']);
            $user = User::findIdentity($custom->site_user_id);
            if($user){
                // New obj and set params
                $ipn = new PayForm();
                $ipn->setParams($user);

                // Sending request to PayPal using CURL
                $response = $ipn->sendRequest();

                // Inspect IPN validation result and act accordingly
                // Split response body and payload.
                if (preg_match('/VERIFIED/', $response)) {
                    Payments::setPayment($post, true, $user);
                }
            }
        }
        
    }

    public function actionSuccess()
    {
        if (Yii::$app->request->isPost) {
            $model = Payments::setPayment(Yii::$app->request->post());

            return $this->render('success', [
                'model' => $model,
            ]);
        }

        return $this->goHome();
    }

    public function actionCancel()
    {
        if (Yii::$app->request->isPost) {
            return $this->render('cancel');
        }

        return $this->goHome();
    }
}