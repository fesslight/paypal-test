<?php
namespace frontend\controllers;


use Yii;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use frontend\models\PayForm;
use common\models\Payment;
use common\models\User;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\LoginForm;
use frontend\models\SignupForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $auth = ['index', 'logout', 'download-zip'];
        $no_auth = ['signup', 'login'];

        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ArrayHelper::merge($auth, $no_auth),
                'rules' => [
                    [
                        'actions' => $no_auth,
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => $auth,
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $payment = User::find()->where(['id' => Yii::$app->user->id])->andWhere(['>=', 'money', PayForm::COUNT_SUM_PAID])->one();

        if($payment){
            return $this->render('download');
        }else{
            $pay_form = new PayForm();
            $pay_form->amount = PayForm::COUNT_SUM_PAID;
            $pay_form->setParams();

            return $this->render('index', ['model' => $pay_form]);
        }
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     *
     * Download page after payment
     *
     * */
    public function actionDownloadZip()
    {
        $payment = User::find()->where(['id' => Yii::$app->user->id])->andWhere(['>=', 'money', PayForm::COUNT_SUM_PAID])->one();


        if($payment){
            $file = Yii::getAlias('files/') . 'hello.rar';
            
            return Yii::$app->response->sendFile($file);
        }else{
            throw new AccessDeniedException('No access to download file');
        }
    }
    
    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
}
