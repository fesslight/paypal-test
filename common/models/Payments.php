<?php

namespace common\models;

use Yii;
use common\models\User;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "payment".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $status
 * @property double $amount
 * @property string $transaction_token
 * @property integer $transaction_time
 *
 * @property User $user
 */
class Payments extends ActiveRecord
{
    /* PayPal Status */
    const STATUS_CANCELED = 0;
    const STATUS_COMPLETED = 1;
    const STATUS_DECLINED = 2;
    const STATUS_EXPIRED = 3;
    const STATUS_FAILED = 4;
    const STATUS_IN_PROGRESS = 5;
    const STATUS_PARTIALLY_REFUNDED = 6;
    const STATUS_PENDING = 7;
    const STATUS_PROCESSING = 8;
    const STATUS_REFUNDED = 9;
    const STATUS_REVERSED = 10;
    const STATUS_VOIDED = 11;

    public static $statuses = [
        self::STATUS_CANCELED => "Canceled_Reversal",
        self::STATUS_COMPLETED => "Completed",
        self::STATUS_DECLINED => "Declined",
        self::STATUS_EXPIRED => "Expired",
        self::STATUS_FAILED => "Failed",
        self::STATUS_IN_PROGRESS => "In-Progress",
        self::STATUS_PARTIALLY_REFUNDED => "Partially_Refunded",
        self::STATUS_PENDING => "Pending",
        self::STATUS_PROCESSING => "Processed",
        self::STATUS_REFUNDED => "Refunded",
        self::STATUS_REVERSED => "Reversed",
        self::STATUS_VOIDED => "Voided",
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payments}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'status', 'transaction_time'], 'integer'],
            [['amount'], 'number'],
            [['transaction_token'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'status' => 'Status',
            'amount' => 'Amount',
            'transaction_token' => 'Transaction Token',
            'transaction_time' => 'Transaction Time',
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['transaction_time'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['transaction_time'],
                ],
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function getStatus($post, $complete)
    {
        if ($status = array_search($post['payment_status'], self::$statuses)) {
            $status = ($complete) ? $status : self::STATUS_PROCESSING;
            return $status;
        }
        return self::STATUS_IN_PROGRESS;
    }

    public static function getPayment($post)
    {
        return self::findOne(['transaction_token' => $post['txn_id']]);
    }

    /**
     *
     * Create and update transaction.
     * Update user balance.
     *
     * */
    public static function setPayment($post, $complete = false, $user = null)
    {
        // if transaction exist
        if ($model = self::getPayment($post)) {
            // if payment success
            if ($complete) {
                // update status
                $model->status = self::STATUS_COMPLETED;
                $model->save();
            } else {
                return $model;
            }
        // if transaction not exist
        } else {

            // parsing request data
            $custom = json_decode($post['custom']);
            // find user from request data
            $user = User::findIdentity($custom->site_user_id);
            if (!$user) {
                return false;
            }

            // create new transaction and load property
            $model = new self();
            $model->transaction_token = $post['txn_id'];
            $model->user_id = $user->id;
            $model->amount = $post['mc_gross'];

            if ($complete)
                $model->status = self::STATUS_COMPLETED;
            else
                $model->status = self::getStatus($post, $complete);

            $model->save();
        }

        // update user balance
        if ($model->status == self::STATUS_COMPLETED) {
            $user->incomingMoney($post['mc_gross']);
            $user->save();
        }

        return $model;
    }

}
