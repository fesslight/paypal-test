<?php

use yii\db\Migration;

class m161109_105321_payments extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%payments}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'status' => $this->integer(),
            'amount' => $this->float(),
            'transaction_token' => $this->string(255),
            'transaction_time' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('FK_payments_user_id', 'payments', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('FK_payments_user_id', 'payments');
        $this->dropTable('{{%payments}}');
    }
}
